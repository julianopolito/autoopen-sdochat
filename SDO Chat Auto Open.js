(function() {
    
    /**
     * @function buildBindId
     * @param {Object} context
     * @description Create unique bind ID based on the campaign and experience IDs.
     */
    function buildBindId(context) {
        return `${context.campaign}:${context.experience}`;
    }

    function apply(context, template) {
        const contentZoneSelector = Evergage.getContentZoneSelector(context.contentZone);
        
        return Evergage.DisplayUtils
            .bind(buildBindId(context))
            .pageElementLoaded("#helpButtonSpan > span.message")
            .then((element) => {
                //const html = template(context);
                //Evergage.cashDom("body").append(html);
                console.log("embedded_sc found", Evergage.cashDom());
                if(embedded_svc){
                    setTimeout(function(){
                            embedded_svc.inviteAPI.inviteButton.acceptInvite();

                            const stat = {
                                experienceId: context.experience,
                                stat: "Impression",
                                control: (context.userGroup != "testUserGroup")
                            }

                        Evergage.sendStat({ campaignStats: [stat] });



                        },1000);
                }else{
                    console.log("failed to find embedded_svc");
                }
            });
    }

    function reset(context, template) {

    }

    function control(context) {
        
    }

    registerTemplate({
        apply: apply,
        reset: reset,
        control: control
    });

})();
